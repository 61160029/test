package oxgame;
import java.util.Scanner;

public class Game {
    Board board;
    Player x;
    Player o;
    int row,col;
    
    Game(){
        o = new Player('O');
        x = new Player('X');
        board = new Board(x,o);
    }
    public void startGame(){
        showWelcome();
        showBoard();
        while (true) {
            showTurn();
            addInput();
            setBoard();
            if (board.theEnd()) {
		showBoard();
		showResult();
		break;
            }
            if (board.getCount() >= 9) {
		showBoard();
		showResult();
		break;
            }
            getPlayer();
            showBoard();
	}
    }   
    
    public void showWelcome(){
        System.out.println("Game XO,Come to play!");
    }
    public void showBoard(){
        System.out.println("\n\t\t|-----------|");
	System.out.printf("\t\t| %c | %c | %c |\n" , board.Table[0][0] , board.Table[0][1] , board.Table[0][2]);
	System.out.print("\t\t|---+---+---|\n");
	System.out.printf("\t\t| %c | %c | %c |\n" , board.Table[1][0] , board.Table[1][1] , board.Table[1][2]);
	System.out.print("\t\t|---+---+---|\n");
	System.out.printf("\t\t| %c | %c | %c |\n" , board.Table[2][0] , board.Table[2][1] , board.Table[2][2]);
        System.out.println("\t\t|-----------|");
    }
    public void showTurn(){
        System.out.print("Player"+board.getPlayer()+",please enter the number : ");
    }
    public void getPlayer(){
        board.changeTurn();
    }
    public void addInput(){
        Scanner kb = new Scanner(System.in);
        int rc = kb.nextInt();
        row = board.getRow(rc);
        col = board.getCol(rc);
    }
    public void setBoard(){
	if (board.Table[row][col] == '1' || board.Table[row][col] == '2' || board.Table[row][col] == '3' || board.Table[row][col] == '4' || board.Table[row][col] == '5' || board.Table[row][col] == '6' || board.Table[row][col] == '7' || board.Table[row][col] == '8' || board.Table[row][col] == '9'){
		board.Table[row][col] = (char)board.getCurrent().getName();
	} 
	else {
		System.out.println("!!! ERROR !!! ,Please enter the number");
		showTurn();
		addInput();
	}
    }
    public void showResult() {
	if (board.theEnd()) {
		System.out.println("Congratuiations!!, Player " + board.getPlayer() + " [" + board.getCurrent().getName() + "] " + ", Winner Winner Chicken Dinner");
	} 
	else {
		System.out.printf("\tIt's a draw!\n");
	}
    }
}
