import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import oxgame.*;


public class TestBoard {
    
    public TestBoard() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void testChangePlayer1(){
        Player o = new Player('o');
        Player x = new Player('x');
        Board board = new Board(o,x);
        board.changeTurn();
        assertEquals('x',board.getCurrent().getName());
    }
    
    @Test
    public void testChangePlayer2(){
        Player o = new Player('o');
        Player x = new Player('x');
        Board board = new Board(o,x);
        board.changeTurn();
        assertEquals('o',board.getCurrent().getName());
    }
    
    @Test
    public void testRow1Win(){
        Player o = new Player('o');
        Player x = new Player('x');
        Board board = new Board(o,x);
        board.getCol(1);
        board.getRow(1);
        board.getCol(2);
        board.getRow(1);
        board.getCol(3);
        board.getRow(1);
        assertEquals(true,board.theEnd());
    }
    
    @Test
    public void testCol1Win(){
        Player o = new Player('o');
        Player x = new Player('x');
        Board board = new Board(o,x);
        board.getCol(1);
        board.getRow(1);
        board.getCol(1);
        board.getRow(2);
        board.getCol(1);
        board.getRow(3);
        assertEquals(true,board.theEnd());
    }
    
    @Test
    public void testRow2Win(){
        Player o = new Player('o');
        Player x = new Player('x');
        Board board = new Board(o,x);
        board.getCol(1);
        board.getRow(2);
        board.getCol(2);
        board.getRow(2);
        board.getCol(3);
        board.getRow(2);
        assertEquals(true,board.theEnd());
    }
    
    @Test
    public void testCol2Win(){
        Player o = new Player('o');
        Player x = new Player('x');
        Board board = new Board(o,x);
        board.getCol(2);
        board.getRow(1);
        board.getCol(2);
        board.getRow(2);
        board.getCol(2);
        board.getRow(3);
        assertEquals(true,board.theEnd());
    }
    
    @Test
    public void testRow3Win(){
        Player o = new Player('o');
        Player x = new Player('x');
        Board board = new Board(o,x);
        board.getCol(1);
        board.getRow(3);
        board.getCol(2);
        board.getRow(3);
        board.getCol(3);
        board.getRow(3);
        assertEquals(true,board.theEnd());
    }
    
    @Test
    public void testCol3Win(){
        Player o = new Player('o');
        Player x = new Player('x');
        Board board = new Board(o,x);
        board.getCol(3);
        board.getRow(1);
        board.getCol(3);
        board.getRow(2);
        board.getCol(3);
        board.getRow(3);
        assertEquals(true,board.theEnd());
    }
}
